var gulp = require('gulp');
var browserSync = require('browser-sync').create();

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./",
            routes: {
                "/console": "index.html",
                "/vconsole": "index.html",
                "/alert": "index.html",
            },
        },
        startPath: '/console',
    });
});


gulp.task('default', ['browser-sync']);
