import {truth} from "./truth.js";

export class HelloModule {
    constructor() {
        this.hello = "Hello from module!";
    }

    sayHello() {
        return this.hello;
    }
    getTruth() {
        return truth;
    }
}
