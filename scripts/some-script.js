var scriptGlobalVar = "This is a ES5 global var defined with 'var' on 'scripts/some-script.js'.";
let scriptGlobalVarES6 = "This is a ES6 global var defined with 'let' on 'scripts/some-script.js'.";
const scriptGlobalConst = "This is a ES6 global const defined with ES6'const' on 'scripts/some-script.js'.";

class HelloScript {
    constructor() {
        this.hello = "Hello from script!";
    }

    sayHello() {
        return this.hello;
    }
}
